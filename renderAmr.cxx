#include <iostream>
#include <vtkm/cont/BoundsCompute.h>
#include <vtkm/source/Amr.h>
#include <vtkm/filter/multi_block/AmrArrays.h>
#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/CellClassification.h>
#include <vtkm/cont/DataSetBuilderUniform.h>
using namespace std;

namespace vtkm
{
namespace worklet
{


// this worklet sets the blanked bit to zero,
// it discards all old information on blanking whil keeping the other bits, e.g., for ghost cells
struct myResetGhostTypeWorklet : vtkm::worklet::WorkletMapField
{
  using ControlSignature = void(FieldIn ghostArrayIn, FieldOut ghostArrayOut);
  using ExecutionSignature = void(_1, _2);
  using InputDomain = _1;
  VTKM_EXEC void operator()(UInt8 ghostArrayIn, UInt8& ghostArrayOut) const
  {
    ghostArrayOut = ghostArrayIn & ~vtkm::CellClassification::Blanked;
  }
};


//template <typename FloatType>
class mySampleCellAssocCells : public vtkm::worklet::WorkletMapField
{

public:
  using ControlSignature = void(FieldIn,
                                WholeArrayIn,
                                WholeArrayIn);//10
  using ExecutionSignature = void(_1, _2, _3);//11

  template <typename ScalarPortalType, typename GhostPortalType>
  VTKM_EXEC inline void operator()(const vtkm::Id& currentCell,
                                   ScalarPortalType& scalarPortal,
                                   GhostPortalType& ghostPortal) const//11
  {
    std::cout<<int(scalarPortal.Get(currentCell))<<std::endl;
//    if (int(scalarPortal.Get(0)) != 0)
//      return;
//    if (int(ghostPortal.Get(0)) != 0)
//      return;
  }
 
}; //class Sample cell

} // worklet
} // vtkm

int main(int argc, char* argv[])
{
  // Verify input arguments
  if (argc != 2)
  {
    std::cout << "Usage: " << argv[0] << " OuputFileName.png " << std::endl;
    return EXIT_FAILURE;
  }
  std::string outputFilename = argv[1];
  
  
  int dim = 3;
  int numberOfLevels = 3;
  int cellsPerDimension = 2;
  std::string fieldName = "RTDataCells";
//  std::string fieldName = "RTData";

  // Generate AMR
  vtkm::cont::SetGlobalGhostCellFieldName("vtkGhostType");
  vtkm::source::Amr source;
  source.SetDimension(dim);
  source.SetCellsPerDimension(cellsPerDimension);
  source.SetNumberOfLevels(numberOfLevels);
  vtkm::cont::PartitionedDataSet partitionedData = source.Execute();
  
//  vtkm::cont::ArrayHandle<vtkm::Range> ghostRange = vtkm::cont::FieldRangeCompute(partitionedData, "vtkGhostType");
//  std::cout<<"ghostRange "<<ghostRange.ReadPortal().Get(0)<<std::endl;
  
  // Generate helper arrays
  std::cout << "Generate" << std::endl;
  vtkm::filter::multi_block::AmrArrays amrArrays;
  auto derivedPartitionedData = amrArrays.Execute(partitionedData);
  //  derivedPartitionedData = partitionedData;
//  partitionedData.PrintSummary(cout);
  vtkm::cont::DataSet partition = partitionedData.GetPartition(0);

  vtkm::cont::Invoker invoke;
  vtkm::cont::ArrayHandle<vtkm::UInt8> ghostField;

  // arrayHandle works
//  invoke(vtkm::worklet::myResetGhostTypeWorklet{}, partition.GetField("vtkGhostType", vtkm::cont::Field::Association::Cells).GetData().AsArrayHandle<vtkm::cont::ArrayHandle<vtkm::UInt8>>(), ghostField);
//  partition.AddCellField("vtkGhostType", ghostField);
//  printSummary_ArrayHandle(partition.GetField("vtkGhostType", vtkm::cont::Field::Association::Cells).GetData().AsArrayHandle<vtkm::cont::ArrayHandle<vtkm::UInt8>>(), cout);

  // arrayHandleConstant works
//  partition.AddCellField("vtkGhostType", vtkm::cont::ArrayHandleConstant<vtkm::UInt8>(9, partition.GetNumberOfCells()));
//  printSummary_ArrayHandle(partition.GetField("vtkGhostType", vtkm::cont::Field::Association::Cells).GetData().AsArrayHandle<vtkm::cont::ArrayHandleConstant<vtkm::UInt8>>(), cout);
//  invoke(vtkm::worklet::myResetGhostTypeWorklet{}, partition.GetField("vtkGhostType", vtkm::cont::Field::Association::Cells).GetData().AsArrayHandle<vtkm::cont::ArrayHandleConstant<vtkm::UInt8>>(), ghostField);
//  partition.AddCellField("vtkGhostType", ghostField);
//  printSummary_ArrayHandle(partition.GetField("vtkGhostType", vtkm::cont::Field::Association::Cells).GetData().AsArrayHandle<vtkm::cont::ArrayHandle<vtkm::UInt8>>(), cout);
  
  // but how can I call it without knowing the kind?
//  partition.AddCellField("vtkGhostType", vtkm::cont::ArrayHandleConstant<vtkm::UInt8>(9, partition.GetNumberOfCells()));
//  printSummary_ArrayHandle(partition.GetField("vtkGhostType", vtkm::cont::Field::Association::Cells).GetData().ExtractComponent<vtkm::UInt8>(0), cout);
//  invoke(vtkm::worklet::myResetGhostTypeWorklet{}, partition.GetField("vtkGhostType", vtkm::cont::Field::Association::Cells).GetData().ExtractComponent<vtkm::UInt8>(0), ghostField);
//  partition.AddCellField("vtkGhostType", ghostField);
//  printSummary_ArrayHandle(partition.GetField("vtkGhostType", vtkm::cont::Field::Association::Cells).GetData().ExtractComponent<vtkm::UInt8>(0), cout);
  
  // now the difficult one
//  partition.AddCellField("vtkGhostType", vtkm::cont::ArrayHandleConstant<vtkm::UInt8>(9, partition.GetNumberOfCells()));
  invoke(vtkm::worklet::mySampleCellAssocCells{}, vtkm::cont::ArrayHandleIndex(partition.GetNumberOfCells()), partition.GetField("vtkGhostType", vtkm::cont::Field::Association::Cells).GetData().ExtractComponent<vtkm::UInt8>(0), partition.GetField("vtkGhostType", vtkm::cont::Field::Association::Cells).GetData().ExtractComponent<vtkm::UInt8>(0));
//  partition.AddCellField("vtkGhostType", ghostField);
  printSummary_ArrayHandle(partition.GetField("vtkGhostType", vtkm::cont::Field::Association::Cells).GetData().ExtractComponent<vtkm::UInt8>(0), cout);

}


